from xml.etree.ElementTree import canonicalize
from fastapi import FastAPI
import psycopg
from pydantic import BaseModel

app = FastAPI()

@app.get("/hello-world")
def root():
    return {"message": "Hello World"}

# GET path /api/categories
@app.get("/api/categories")
def list_categories(page: int=0):
    #                         username    password        database
    conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute("""
            SELECT id, title, canon
            FROM categories 
            LIMIT 100 OFFSET %s
            """, [page * 100])            
            records = cur.fetchall()
            results = []
            for record in records:
                # record = [1, ""!"", true]
                result = {
                    "id": record[0],
                    "title": record[1],
                    "canon": record[2],
                }
                results.append(result)
            return results

# /api/categories/7
@app.get("/api/categories/{category_id}")
def get_category(category_id: int):
    conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute("""
            SELECT id, title, canon
            FROM categories 
            WHERE id = %s  # %s means to Interpolate a string
            """, [category_id])         
            record = cur.fetchone()
            return {
                "id": record[0],
                "title": record[1],
                "canon": record[2],
            }


class Category(BaseModel):
     title: str
     canon: bool


@app.post("/api/categories")
def create_category(category: Category):
    print(category)
    return "YAY!"



